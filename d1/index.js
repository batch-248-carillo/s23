console.log("Hello, B248!");

//Objects

/*
	An object is a data type that is used to represent real world objects
	**It is also a collection of related data and or functionalities

	Information stored in objects are represented in a key:value pair

	"key" - "property" of an object
	"value" - actual data to be stored

	**different data type may be stored in an object's property creating complex data structures

*/

//2 ways in creating objects in JS

//1. Object Literal Notation/
	//let object = {}; (curly braces)
//2. Object Constructor Notation/
	//Object Instantation
	//let object = new Object()


//Object Literal Notation
	//creating objects using initializer/literal notation
	//camelCase
	//A cellphone is an example of a real world object
	//It has it's own properties such as name, color, weight, unit model and a lot of other things
	/*
		Syntax:

		let objectName = {
			keyA : valueA,
			keyB : valueB
		};
	*/

	
	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	};

	console.log("Result from creating objects using literal notation: ");
	console.log(cellphone);

	let cellphone2 = {
		name: "iPhone 14",
		manufactureDate: 2023
	}

	console.log("Result from creating objects using literal notation: ");
	console.log(cellphone2);

	let ninja = {
		name: "Naruto Uzumaki",
		village: "Konoha",
		children: ["Boruto", "Himawari"]
	};
	console.log(ninja);

	/*

		Mini Activity
		create an object called "bootcamper" (3 min 6:50PM)

		Your:

		name
		address
		hobbies
		favorite movie
		note to self

	*/

	let bootcamper = {
		name: "Cee",
		address: "South Korea",
		hobbies: ["Playing LOL","Digital Art"],
		favoriteMovie: "The Pianist",
		noteToSelf: "'di lang kakayanin, kayang-kaya!"

	}

	console.log(bootcamper);

	//Object Constructor Notation
		//Creating objects using a constructor function
		//creates a REUSABLE function to create several objects that have the same data structure
		//This is useful for creating multiple instances/copies of an object
		//An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it

		/*
			Syntax

			function objectName(keyA,keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}
		*/

			//this keyword refers to the properties within the object
				//it allows the assignment of new objects' properties by associating them with the values received from the constructor function's parameter



			//constructor serves a blueprint
			function Laptop(name,manufactureDate){
				this.name = name;
				this.manufactureDate = manufactureDate;
			}

			//create an instance object using the Laptop constructor

			let laptop = new Laptop("Lenovo", 2008);

			console.log("Result from creating objects using object constructor");
			console.log(laptop);

			//the "new" operator creates an instance of an object (new object)
			let myLaptop = new Laptop("Macbook Air", 2020);

			console.log("Result from creating objects using object constructor");
			console.log(myLaptop);

			//Mini Activity #2
			//create 3 more instances of our Laptop constructor

			let laptop1 = new Laptop("Asus",2020);
			let laptop2 = new Laptop("Acer",2021);
			let laptop3 = new Laptop("HP",2023);
			console.log(laptop1);
			console.log(laptop2);
			console.log(laptop3);


			//without the new keyword

			let oldLaptop = Laptop("Portal R2E CCMC",1980);
			console.log("Result from creating objects using object constructor");
			console.log(oldLaptop);//undefined
			
			//Create empty objects

			let computer = {};
			let myComputer = new Object();

			console.log(computer);
			console.log(myComputer);


//Accesing Object Properties

		//1. dot notation
			console.log("Result from dot notation: " + myLaptop.name);
		//2. square bracket notation
			console.log("Result from square notation: " + myLaptop["name"]);

			console.log(ninja.name);
			console.log(ninja.village);
			console.log(ninja.children[1]);


//Accessing array of objects
	//accessing objecy properties using the square bracket notation and array indexes can cause confusion


	let arrayObj = [laptop,myLaptop];
	//may be confused for accessing array indexes
	console.log(arrayObj[0]["name"]);
	//this tells us that array[0] is an object by using the dot notation
	console.log(arrayObj[0].name);

	//Initialize/Add/Delete/Reassign Object Properties
	/*
		While using the square bracket, will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accessed using the square bracket notation
		This also makes names of object properties to not follow commonly used naming conventions for them

	*/

	//create an object using object literals

	let car = {};
	console.log("Current value of car object");
	console.log(car);//{}

	//initializing/adding object properties
	car.name = "Honda Civic";
	console.log("Result from adding properties using dot notation: ");
	console.log(car);

	car["manufacture date"] = 2019;
	console.log("Result from adding properties using square bracket notation: ");
	console.log(car);

	delete car["manufacture date"];
	console.log("Result from deleting properties: ");
	console.log(car);

	car["manufactureDate"] = 2019; //added a property
	console.log(car);

	//Reassigning object property values

	car.name = "Toyota Vios";
	console.log("Result from reassigning property values: ");
	console.log(car);

	delete car.manufactureDate;
	console.log(car);


	//Object Methods
		//a method is a function which is a property of an object

	let person = {
		name: "Cardo Dalisay",
		talk: function(){
			console.log("Hello, my name is " + this.name);
		}
	}
	console.log(person);
	console.log("Result from object method: ");
	person.talk();

	person.walk = function(steps){
		console.log(this.name + " walked " + steps + " steps forward");
	}

	console.log(person);
	person.walk(500);


	let friend = {

		firstName: "Tom",
		lastName: "Sawyer",
		address: {
			city: "Manila",
			country: "Philippines"
		},
		emails: ["sawyer@mail.com","tomsawyer@gmail.com"],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". " + "I live in " + this.address.city + ", " + this.address.country);
		}

	}

	friend.introduce();


	//REAL WORLD APPLICATION OF OBJECTS
	/*
		Scenario
		1. We would like to create a game that would have several Pokemon interact with each other
		2. Every Pokemon would have the same set of stats, properties, and function

		Stats:
		name:
		level:
		health: level * 2
		attack: level

	*/

	//create an object constructor to lessen the process in creating the pokemon

	function Pokemon(name,level){

		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level;
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);

			//Mini Activity (5 min, 8:34PM)
				
				//reduce the target object's health property by subtracting and reassining its value based on the pokemon's attack
				//target.health = target.health - this.attack
				target.health -= this.attack

				//example if health is not less than 0
					//rattata's health is now reduced to 5

				console.log(target.name + " health is now reduced to " + target.health);

				//if the target's health is less than or equal to 0 we will invoke the faint method, otherwise printout the pokemon's new health

				if(target.health<=0){
					target.faint()
				}

		}

		//create another method called faint
		this.faint = function(){
			console.log(this.name + " fainted.");
		}

	}

	let pikachu = new Pokemon ("Pikachu", 15);
	console.log(pikachu);
	let ratata = new Pokemon("Ratata",10);
	console.log(ratata);

	pikachu.tackle(ratata);